import generate.Echo;
import generate.EchoServiceImplService;
import generate.EchoServiceInterface;

//wsimport -keep -s src/main/java/ -p generate http://localhost:8080/verySimpleService-ejb/EchoService?wsdl -verbose
public class userOfEchoService {

    public static void main(String[] args) {
        System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");

        EchoServiceImplService service = new EchoServiceImplService();
        EchoServiceInterface serviceInterface = service.getEchoServicePort();

        Echo echo = new Echo();
        System.out.println(serviceInterface.echo(echo,"Echo!"));
    }


}
