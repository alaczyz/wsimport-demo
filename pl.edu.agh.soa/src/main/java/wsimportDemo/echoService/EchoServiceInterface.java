package wsimportDemo.echoService;

import javax.ejb.Stateless;
import javax.jws.WebParam;
import javax.jws.WebService;

@Stateless
@WebService()
public interface EchoServiceInterface {
    public String echo(@WebParam(name = "someValue")String someValue);
}
