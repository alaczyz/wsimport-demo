package wsimportDemo.echoService;

import javax.ejb.Stateless;
import javax.jws.WebParam;
import javax.jws.WebService;

// SIB Service Implementation Beam
@Stateless
@WebService(endpointInterface = "EchoServiceInterface",
            name = "EchoService",
        portName = "EchoServicePort",
        targetNamespace = "http://pl.edu.agh.soa/echo")
public class EchoServiceImplementation implements EchoServiceInterface {

    public String echo(@WebParam(name = "someValue")String someValue){
        // magic property allows to lgo http messages on standart output
        System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");

        return someValue;
    }
}
