package wsimportDemo.secretInHeaderService;

import javax.ejb.Stateless;
import javax.jws.HandlerChain;
import javax.jws.WebService;

@Stateless
@WebService(
        name = "SecretService",
        portName = "SecretServicePort",
        targetNamespace = "http://lab.soa.org/secret")
@HandlerChain(file="HeaderHandler.xml")
public class SecretService {

    public String tellMeASecret() {
        System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
        return "Don't tell anyone!";
    }
}
