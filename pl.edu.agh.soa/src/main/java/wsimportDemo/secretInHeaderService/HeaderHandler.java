package wsimportDemo.secretInHeaderService;

import javax.xml.namespace.QName;
import javax.xml.soap.Node;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import javax.xml.ws.soap.SOAPFaultException;
import java.util.Iterator;
import java.util.Set;

public class HeaderHandler implements SOAPHandler<SOAPMessageContext> {
        private static final String PASSWORD = "admin123";

        @Override
        public boolean handleMessage(SOAPMessageContext context) {

            // dodamy element do sekcji HEADER w odpowiedzi
            Boolean isResponse = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

            if(!isResponse){

                try{
                    SOAPMessage soapMsg = context.getMessage();
                    SOAPEnvelope soapEnv = soapMsg.getSOAPPart().getEnvelope();
                    SOAPHeader soapHeader = soapEnv.getHeader();

                    //jeśli nie ma header to go dodamy
                    if (soapHeader == null){
                        soapHeader = soapEnv.addHeader();
                        //throw exception
                        generateSOAPErrMessage(soapMsg, "No SOAP header.");
                    }

                    Iterator it = soapHeader.examineAllHeaderElements();

                    if (it == null || !it.hasNext()){
                        generateSOAPErrMessage(soapMsg, "No header block for next actor.");
                    }

                    Node passwordNode = (Node) it.next();
                    String providedPassword = (passwordNode == null) ? null : passwordNode.getValue();

                    if (providedPassword == null){
                        generateSOAPErrMessage(soapMsg, "No password in header block.");
                    }
                    if (!PASSWORD.equals(providedPassword)){
                        generateSOAPErrMessage(soapMsg, "Wrong Password");
                    }


                }catch(SOAPException e){
                    System.err.println(e);
                }

            }

            //continue other handler chain
            return true;
        }

        @Override
        public boolean handleFault(SOAPMessageContext context) {

            System.out.println("Server : handleFault()......");

            return true;
        }

        @Override
        public void close(MessageContext context) {
            System.out.println("Server : close()......");
        }

        @Override
        public Set<QName> getHeaders() {
            System.out.println("Server : getHeaders()......");
            return null;
        }

        private void generateSOAPErrMessage(SOAPMessage msg, String reason) {
            try {
                SOAPBody soapBody = msg.getSOAPPart().getEnvelope().getBody();
                SOAPFault soapFault = soapBody.addFault();
                soapFault.setFaultString(reason);
                throw new SOAPFaultException(soapFault);
            }
            catch(SOAPException e) { }
        }
    }
